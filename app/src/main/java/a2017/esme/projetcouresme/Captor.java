package a2017.esme.projetcouresme;

import android.content.Context;
import android.content.Intent;

import java.util.Random;

import a2017.esme.projetcouresme.interfaces.ICaptor;

/**
 * Created by jips on 10/23/17.
 */

public class Captor implements ICaptor, Runnable{

    float mTemperature = 0;
    int mId = 0;
    Thread mThread;
    Boolean mIsRunning = false;
    Context mContext;

    //constructor
    public Captor(Context context){

        Random rand = new Random();
        mId = rand.nextInt(5000) + 1;

        mIsRunning = true;
        mThread = new Thread(this);
        mThread.start();

        mContext = context;

    }
    @Override
    public void run() {

        double offset = 0.5;
        mTemperature = 16;
        while (mIsRunning){
            mTemperature += offset;
            if(mTemperature == 22){
                offset = -0.5;
            }else if (mTemperature == 16){
                offset = 0.5;
            }

            if(mContext != null){
                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                intent.setAction("com.example.Broadcast");
                intent.putExtra("Temp", mTemperature);
                mContext.sendBroadcast(intent);
            }

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public float getTemperature() {
        return mTemperature;
    }

    @Override
    public int getId() {
        return mId;
    }


}
