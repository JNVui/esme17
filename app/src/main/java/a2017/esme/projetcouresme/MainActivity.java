package a2017.esme.projetcouresme;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.nfc.Tag;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import a2017.esme.projetcouresme.interfaces.ICentral;
import a2017.esme.projetcouresme.interfaces.IDataNotify;

public class MainActivity extends AppCompatActivity implements IDataNotify {

    final String TAG = "MainActivity";
    TextView mTempRoom1, mTempRoom2, mTempRoom3, mTempRoom4;
    Button mButtonSync;
    Button mButtonConnection;

    Handler mHandler = new Handler();

    Runnable mRunnable = new Runnable() {
        @Override
        public void run() {

                mTempRoom2.setText(""+mCentral.getTemperature(2));
                mTempRoom4.setText(""+mCentral.getTemperature(4));

                mHandler.postDelayed(this,1000);

        }
    };

    ICentral mCentral;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTempRoom1 = (TextView) findViewById(R.id.room_one_temp);
        mTempRoom2 = (TextView) findViewById(R.id.room_two_temp);
        mTempRoom3 = (TextView) findViewById(R.id.room_three_temp);
        mTempRoom4 = (TextView) findViewById(R.id.room_four_temp);

        mButtonSync = (Button) findViewById(R.id.button_sync);
        mButtonConnection = (Button) findViewById(R.id.button_connect);

        mButtonConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCentral.connection(MainActivity.this);
                mHandler.post(mRunnable);
            }
        });


        mButtonSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
                intent.setAction("com.example.Broadcast");

                intent.putExtra("Temp", 33);

                sendBroadcast(intent);

            }
        });


        IntentFilter filter = new IntentFilter("com.example.Broadcast");
        MyBroadcastReceiver receiver = new MyBroadcastReceiver();
        registerReceiver(receiver, filter);


        mCentral = new Central(this);
        Log.i(TAG,"onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.i(TAG,"onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.i(TAG,"onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.i(TAG,"onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.i(TAG,"onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.i(TAG,"onDestroy");
    }

    @Override
    public void dataNotify(int idCaptor, double temperature) {
        Log.i(TAG,"id = "+idCaptor + "\n temp = "+temperature);
    }




    public static class MyBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            Toast.makeText(context, "Temp = "+intent.getFloatExtra("Temp",0), Toast.LENGTH_SHORT).show();
            Log.i("MyBroadcastReceiver", "Temp = "+intent.getFloatExtra("Temp",0));
        }
    }




}
