package a2017.esme.projetcouresme.interfaces;

/**
 * Created by jips on 10/23/17.
 */

public interface ICaptor {

    float getTemperature();
    int getId();
}
