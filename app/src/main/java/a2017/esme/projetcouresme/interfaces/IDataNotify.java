package a2017.esme.projetcouresme.interfaces;

/**
 * Created by jips on 11/6/17.
 */

public interface IDataNotify {

    void dataNotify(int idCaptor, double temperature);

}
