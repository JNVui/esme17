package a2017.esme.projetcouresme.interfaces;

/**
 * Created by jips on 10/16/17.
 */

public interface ICentral {

    void connection(IDataNotify dataNotify);
    float getTemperature(int captorId);

}
