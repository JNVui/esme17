package a2017.esme.projetcouresme;

import java.util.Random;

import a2017.esme.projetcouresme.interfaces.ICaptor;
import a2017.esme.projetcouresme.interfaces.IDataNotify;

/**
 * Created by jips on 10/23/17.
 */

public class Captor2 implements ICaptor, Runnable{

    float mTemperature = 0;
    int mId = 0;
    Thread mThread;
    Boolean mIsRunning = false;
    IDataNotify mDataNotify;

    //constructor
    public Captor2(IDataNotify dataNotify){

        mDataNotify = dataNotify;
        Random rand = new Random();
        mId = rand.nextInt(5000) + 1;

        mIsRunning = true;
        mThread = new Thread(this);
        mThread.start();


    }
    @Override
    public void run() {

        double offset = 0.5;
        mTemperature = 10;
        while (mIsRunning){
            mTemperature += offset;
            if(mTemperature == 16){
                offset = -0.5;
            }else if (mTemperature == 10){
                offset = 0.5;
            }

            mDataNotify.dataNotify(mId,mTemperature);

            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public float getTemperature() {
        return mTemperature;
    }

    @Override
    public int getId() {
        return mId;
    }


}
