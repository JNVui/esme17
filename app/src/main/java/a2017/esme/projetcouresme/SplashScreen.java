package a2017.esme.projetcouresme;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;


public class SplashScreen extends AppCompatActivity {

    private static final long DEFAULT_DELAY = 5000;
    ImageView mImageView;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        mImageView = (ImageView) findViewById(R.id.image);

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                new DownloadPicture().execute("https://api.learn2crack.com/android/images/donut.png");
            }
        }, 1000);

    }

    public void startMain(){
        startActivity(new Intent(this,MainActivity.class));
        finish();
    }

    private class  DownloadPicture extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... params) {


            String imageURL = params[0];
            Bitmap bitmap = null;

            try {
                // Download Image from URL
                InputStream input = new java.net.URL(imageURL).openStream();

                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }


        @Override
        protected void onPostExecute(Bitmap bitmap) {

             mImageView.setImageBitmap(bitmap);

            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    startMain();
                }
            };
            Timer timer = new Timer();
            timer.schedule(timerTask, DEFAULT_DELAY);
        }
    }

}

