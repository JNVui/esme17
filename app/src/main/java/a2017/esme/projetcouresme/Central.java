package a2017.esme.projetcouresme;

import android.content.Context;

import a2017.esme.projetcouresme.interfaces.ICaptor;
import a2017.esme.projetcouresme.interfaces.ICentral;
import a2017.esme.projetcouresme.interfaces.IDataNotify;

/**
 * Created by jips on 10/16/17.
 */

public class Central implements ICentral {

    ICaptor mCaptor1,mCaptor2,mCaptor3,mCaptor4;
    Context mContext;

    public Central(Context context){
        mContext = context;
    }

    @Override
    public void connection(IDataNotify dataNotify) {

        if(mCaptor1 == null){
            mCaptor1 = new Captor2(dataNotify);
            mCaptor2 = new Captor(mContext);
            mCaptor3 = new Captor2(dataNotify);
            mCaptor4 = new Captor(mContext);
        }

    }

    @Override
    public float getTemperature(int captorId) {

        switch (captorId){
            case 1:
                return mCaptor1.getTemperature();
            case 2:
                return mCaptor2.getTemperature();
            case 3:
                return mCaptor3.getTemperature();
            default:
                return mCaptor4.getTemperature();
        }

    }

}
